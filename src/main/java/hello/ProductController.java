package hello;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Products
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class ProductController {

	private static final AtomicLong counter = Database.getProductCounter();
    private static Map<Long, Product> productDb = Database.getProductDb();
    
    /**
     * Create a new product in the database
     * @param product product with SKU and description
     * @return the product number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/products/new")
    public long addNewProduct(@RequestBody Product product) {
    	product.setSku(counter.incrementAndGet());
    	productDb.put(product.getSku(), product);
    	return product.getSku();
    }
    
    /**
     * Get a product from the database
     * @param sku the SKU of the product
     * @return the product if in the database, not found if not
     */
    @GetMapping("/products/{sku}")
    public ResponseEntity<Object> getProductBySku(@PathVariable long sku) {
    	if (productDb.containsKey(sku)) {
            return new ResponseEntity<>(productDb.get(sku), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Product does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
   
}