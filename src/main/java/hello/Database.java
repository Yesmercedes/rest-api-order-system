package hello;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Provides an in-memory database for this example
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
public class Database {
	
    private static Map<Long, Customer> customerDb = new HashMap<Long, Customer>();
	private static final AtomicLong customerCounter = new AtomicLong();
    private static Map<Long, Order> orderDb = new HashMap<Long, Order>();
	private static final AtomicLong orderCounter = new AtomicLong();
    private static Map<Long, Product> productDb = new HashMap<Long, Product>();
	private static final AtomicLong productCounter = new AtomicLong();
    
    /**
     * Get the Customer database
     * @return the customer database
     */
    public static Map<Long, Customer> getCustomerDb() {
    	return customerDb;
    }

    /**
     * Get the Customer counter
     * @return the customer counter
     */
    public static AtomicLong getCustomerCounter() {
    	return customerCounter;
    }
    
    /**
     * Get the Order database
     * @return the order database
     */
    public static Map<Long, Order> getOrderDb() {
    	return orderDb;
    }
   

    /**
     * The Order counter
     * @return the order counter
     */
    public static AtomicLong getOrderCounter() {
    	return orderCounter;
    }
    
    /**
     * Get the Product database
     * @return the product database
     */
    public static Map<Long, Product> getProductDb() {
    	return productDb;
    }    
    
    /**
     * Get the Product counter
     * @return the product counter
     */
    public static AtomicLong getProductCounter() {
    	return productCounter;
    }

}
